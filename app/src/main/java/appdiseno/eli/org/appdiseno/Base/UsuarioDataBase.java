package appdiseno.eli.org.appdiseno.Base;

/**
 * Created by Eliseo on 07/06/2016.
 */
public class UsuarioDataBase {
        private int id;
        private String municipio;
        private String provincia;
        private String calle;
        private String cPostal;
        private String telefono;
        private String comentarios;

        public UsuarioDataBase(int id, String municipio, String provincia, String calle, String cPostal, String telefono, String comentarios){
            this.id=id;
            this.municipio=municipio;
            this.provincia=provincia;
            this.calle=calle;
            this.cPostal=cPostal;
            this.telefono=telefono;
            this.comentarios=comentarios;
        }

    public UsuarioDataBase(String municipio, String provincia, String calle, String cPostal, String telefono, String comentarios){
        this.id=-1;
        this.municipio=municipio;
        this.provincia=provincia;
        this.calle=calle;
        this.cPostal=cPostal;
        this.telefono=telefono;
        this.comentarios=comentarios;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getcPostal() {
        return cPostal;
    }

    public void setcPostal(String cPostal) {
        this.cPostal = cPostal;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    @Override
    public String toString() {
        return "UsuarioDataBase{" +
                "id=" + id +
                ", municipio='" + municipio + '\'' +
                ", provincia='" + provincia + '\'' +
                ", calle='" + calle + '\'' +
                ", cPostal='" + cPostal + '\'' +
                ", comentarios='" + comentarios + '\'' +
                '}';
    }
}
