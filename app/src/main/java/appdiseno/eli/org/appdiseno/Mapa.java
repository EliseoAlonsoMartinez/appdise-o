package appdiseno.eli.org.appdiseno;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;

import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


import appdiseno.eli.org.appdiseno.Base.Correos;


public class Mapa extends Activity {
    public static final String URL="https://www.zaragoza.es/buscador/select?wt=json&q=*:*%20AND%20-tipocontenido_s:estatico%20AND%20category:Recursos&fq=temas_smultiple:%28%22Servicios%20Urbanos%22%29%20AND%20subtemas_smultiple:%28%22Oficinas%20de%20Correos%22%29&srsname=wgs84";

    public ArrayList<Correos> listaCorreos;

    private MapView mapView;
    Correos correos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listaCorreos=new ArrayList<>();

        setContentView(R.layout.activity_mapa);




        mapView=(MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {

                TareaDescarga tarea=new TareaDescarga(mapboxMap);
                tarea.execute(URL);
            }
        });
    }

    private void colocarMapa( MapboxMap mapboxMap){
        System.out.println(listaCorreos+ "YEAA");
        //TODO ME MANDA EL VECTOR VACÍO,PERO EN EL METODO RUN,LO COGE BIEN
        for(Correos correos:listaCorreos){
            mapboxMap.addMarker(new MarkerOptions()
                    .position(new LatLng(correos.getLat(),
                            correos.getLon())));
        }
        //mapboxMap.addMarker(new MarkerOptions().position(new LatLng(41.666091752800625, -0.8243193665112536)));

        System.out.println(listaCorreos);
        System.out.println(correos.getLat());
        System.out.println(correos.getLon());
    }

    @Override
    protected void onStart(){
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mapa, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class TareaDescarga extends AsyncTask<String,Void,Void> {
        private boolean error = false;

        private ProgressDialog dialog;
        private  MapboxMap mapboxMap;

        public TareaDescarga( MapboxMap mapboxMap){
            this.mapboxMap = mapboxMap;
        }



        //No me pasa los datos del json,nose si es por el Objeto del json o porque
        @Override
        protected Void doInBackground(String... strings) {
            correos =new Correos();
            String url = strings[0];
            InputStream is = null;
            String resultado = null;
            JSONObject json = null;
            JSONArray jsonArray = null;
            try {

                HttpClient clienteHtp = new DefaultHttpClient();
                HttpGet httpPost = new HttpGet(url);
                HttpResponse respuesta = clienteHtp.execute(httpPost);
                HttpEntity entity = respuesta.getEntity();
                is = entity.getContent();

                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String linea = null;
                while ((linea = br.readLine()) != null) {
                    sb.append(linea + "\n");
                }
                is.close();
                resultado = sb.toString();

                json = new JSONObject(resultado);
                jsonArray = json.getJSONObject("response").getJSONArray("docs");

                //String nombre2 = null;
                String coordenadas = null;
                Correos correos = null;

                for (int i = 0; i < jsonArray.length(); i++) {
                    //nombre2 = jsonArray.getJSONObject(i).getJSONObject("properties").getString("title");

                    coordenadas = jsonArray.getJSONObject(i).getString("coordenadas_p");
                    coordenadas = coordenadas.substring(0, coordenadas.length() - 1);
                    String latolong[] = coordenadas.split(",");
                    correos = new Correos();
                    correos.setLat(Float.parseFloat(latolong[0]));
                    correos.setLon(Float.parseFloat(latolong[1]));

                    listaCorreos.add(correos);




                    System.out.println(listaCorreos);
                    System.out.println(correos.getLat());
                    System.out.println(correos.getLon());
                }

                colocarMapa(mapboxMap);


            } catch (ClientProtocolException cpe) {
                cpe.printStackTrace();
                error = true;

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }
    }
}
