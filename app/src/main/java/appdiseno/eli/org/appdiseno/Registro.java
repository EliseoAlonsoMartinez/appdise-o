package appdiseno.eli.org.appdiseno;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


public class Registro extends Activity implements View.OnClickListener {
    boolean registro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);




        Button btAceptarRegistro=(Button) findViewById(R.id.btAceptarRegistro);
        btAceptarRegistro.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        EditText etNickRegistro=(EditText) findViewById(R.id.etNickRegistro);
        EditText etPasswordRegistro=(EditText) findViewById(R.id.etPasswordRegistro);
        switch (v.getId()){

            case R.id.btAceptarRegistro:

                String nombre=etNickRegistro.getText().toString();
                String contrasena=etPasswordRegistro.getText().toString();

                WebService webService=new WebService();
                webService.execute(nombre, contrasena);
                Toast toast=Toast.makeText(getApplicationContext(),
                        "Bienvenido a tu cuenta, disfruta de ofertas diarias!!!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER|Gravity.CENTER,0,0);
                toast.show();
                startActivity(new Intent(this, Entrada.class));

        }
    }


    private class WebService extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... params) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            registro = restTemplate.getForObject(
            "http://192.168.1.133:8080" + "/add_usuario?nombre=" + params[0] + "&contrasena=" + params[1], Boolean.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid){
            if(!registro){
                Toast toast = Toast.makeText(getApplicationContext(),"Disfruta ahora de tus ofertas exclusivas para ti!!" , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER,0,0);
                toast.show();
            }else{

                Intent intent = new Intent(getApplicationContext(),Entrada.class);
                startActivity(intent);
            }
        }
    }

}