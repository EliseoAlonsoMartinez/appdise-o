package appdiseno.eli.org.appdiseno;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import appdiseno.eli.org.appdiseno.Base.UsuarioDataBase;
import appdiseno.eli.org.appdiseno.DataBase.DataBase;

public class modificar_detalles extends Activity {

    private DataBase db;

    private Button btnModify;
    private EditText etMunicipio;
    private EditText etProvinia;
    private EditText etCalle;
    private EditText etCPostal;
    private EditText etTelefono;
    private EditText etComentarios;
    private int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_detalles);

        Bundle extras = getIntent().getExtras();
        id = extras.getInt("id");

        db = new DataBase(getApplicationContext());

        btnModify = (Button) findViewById(R.id.btModify);
        etMunicipio = (EditText) findViewById(R.id.etModMunicipio);
        etProvinia = (EditText) findViewById(R.id.etModProvincia);
        etCalle = (EditText) findViewById(R.id.etModCalle);
        etCPostal = (EditText) findViewById(R.id.etModCPostal);
        etTelefono = (EditText) findViewById(R.id.etModTelefono);
        etComentarios = (EditText) findViewById(R.id.etModComentarios);

        btnModify.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                UsuarioDataBase user = new UsuarioDataBase(
                        etMunicipio.getText().toString(),
                        etProvinia.getText().toString(),
                        etCalle.getText().toString(),
                        etCPostal.getText().toString(),
                        etTelefono.getText().toString(),
                        etComentarios.getText().toString()
                );

                db.updatePrendas(user, id);
                Toast.makeText(getApplicationContext(), "Detalles modificados", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(modificar_detalles.this, Empieza.class);
                startActivity(intent);
            }
        });

    }

}
