package appdiseno.eli.org.appdiseno.ArrayAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import appdiseno.eli.org.appdiseno.Base.UsuarioDataBase;
import appdiseno.eli.org.appdiseno.Envio;
import appdiseno.eli.org.appdiseno.R;

/**
 * Created by Eliseo on 10/06/2016.
 */
public class CarritoAdapter extends ArrayAdapter<UsuarioDataBase> {

    private Context context;
    private int layoutId;
    private List<UsuarioDataBase> data;

    public CarritoAdapter(Context context, int layoutId, List<UsuarioDataBase> data) {

        super(context, layoutId, data);

        this.context = context;
        this.layoutId = layoutId;
        this.data = data;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        View row = view;
        ItemUsuario item = null;

        if (row == null)
        {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutId, parent, false);

            item = new ItemUsuario();
            item.municipio = (TextView) row.findViewById(R.id.tvMunicipio);
            item.provincia = (TextView) row.findViewById(R.id.tvProvincia);
            item.calle = (TextView) row.findViewById(R.id.tvCalle);
            item.cPostal = (TextView) row.findViewById(R.id.tvcPostal);
            item.telefono = (TextView) row.findViewById(R.id.tvTelefono);
            item.comentarios = (TextView) row.findViewById(R.id.tvComentarios);


            row.setTag(item);
        }
        else
        {
            item = (ItemUsuario) row.getTag();
        }

        UsuarioDataBase user = data.get(position);

        item.municipio.setText(user.getMunicipio());
        item.provincia.setText(user.getProvincia());
        item.calle.setText(user.getCalle());
        item.cPostal.setText(user.getcPostal());
        item.telefono.setText(user.getTelefono());
        item.comentarios.setText(user.getComentarios());

       return row;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public UsuarioDataBase getItem(int posicion) {

        return data.get(posicion);
    }

    static class ItemUsuario {

        TextView municipio;
        TextView provincia;
        TextView calle;
        TextView cPostal;
        TextView telefono;
        TextView comentarios;
    }

}


