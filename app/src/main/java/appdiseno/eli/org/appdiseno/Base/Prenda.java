package appdiseno.eli.org.appdiseno.Base;

/**
 * Created by k3ym4n on 27/05/2016.
 */
public class Prenda {
    private String talla;
    private String color;
    private int cantidad;
    private String estampacion;
    private String prenda;
    private int cantidad_colores;
    private double precio;

    /*public Prenda(){
        System.out.println(cantidad + " " + prenda);
    }*/

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCantidad_colores() {
        return cantidad_colores;
    }

    public void setCantidad_colores(int cantidad_colores) {
        this.cantidad_colores = cantidad_colores;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEstampacion() {
        return estampacion;
    }

    public void setEstampacion(String estampacion) {
        this.estampacion = estampacion;
    }

    public String getPrenda() {
        return prenda;
    }

    public void setPrenda(String prenda) {
        this.prenda = prenda;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    @Override
    public String toString() {
        return "Prenda{" +
                "cantidad=" + cantidad +
                ", talla='" + talla + '\'' +
                ", color='" + color + '\'' +
                ", estampacion='" + estampacion + '\'' +
                ", prenda='" + prenda + '\'' +
                ", cantidad_colores=" + cantidad_colores +
                ", precio=" + precio +
                '}';
    }
}
