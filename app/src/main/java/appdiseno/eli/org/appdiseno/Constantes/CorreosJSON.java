package appdiseno.eli.org.appdiseno.Constantes;

/**
 * Created by Eliseo on 31/05/2016.
 */
public class CorreosJSON {
    final public static String nombreOficina="nombreOficina";
    final public static String latitud="latitud";
    final public static String longitud="longitud";

    public static final String URL="https://www.zaragoza.es/buscador/select?wt=json&srsname=wgs84&q=*:*%20AND%20-tipocontenido_s:estatico%20AND%20category:Recursos&fq=temas_smultiple:%28%22Servicios%20Urbanos%22%29%20AND%20subtemas_smultiple:%28%22Oficinas%20de%20Correos%22%29";

}
