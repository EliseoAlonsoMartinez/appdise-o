package appdiseno.eli.org.appdiseno;

import android.app.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class Login extends Activity implements View.OnClickListener{
    String nombre;
    Boolean registro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button btAceptarLogin=(Button) findViewById(R.id.btAceptarLogin);
        btAceptarLogin.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        EditText etLoginNick=(EditText) findViewById(R.id.etLoginNick);
        EditText etLoginContrasena=(EditText) findViewById(R.id.etLoginContrasena);
        switch (v.getId()){


            case R.id.btAceptarLogin:

                nombre=etLoginNick.getText().toString();
                String contrasena=etLoginContrasena.getText().toString();

                WebService webService=new WebService();
                webService.execute(nombre,contrasena);
        }
    }


    private class WebService extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... params) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            registro = restTemplate.getForObject(
                    "http://192.168.1.133:8080" + "/usuario_login?nombre=" + params[0] + "&contrasena" + params[1], Boolean.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid){
            if(!registro){
                Toast toast = Toast.makeText(getApplicationContext(),"Hola " + nombre + "!!!", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER,0,0);
                toast.show();
                Intent intent = new Intent(getApplicationContext(),Entrada.class);
                startActivity(intent);
            }else{
                Toast toast = Toast.makeText(getApplicationContext(),"Ups, parece que ha habido un error " + nombre , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER,0,0);
                toast.show();
                Intent intent1 = new Intent(getApplicationContext(),Entrada.class);
                startActivity(intent1);
            }
        }
    }
}
