package appdiseno.eli.org.appdiseno.Constantes;

/**
 * Created by k3ym4n on 27/05/2016.
 */
public class Precio {
    public final static double precio_camiseta_manga_corta=3.5;
    public final static double precio_camiseta_manga_larga=4;
    public final static double precio_sudadera_capucha_bolsillo=8;
    public final static double precio_sudadera_sin_capucha_bolsillo=7;
    public final static double precio_pantalon_corto=7;
    public final static double precio_pantalon_largo=12;
    public final static double precio_polo_manga_corta=7;
    public final static double precio_gorra=4;
}
