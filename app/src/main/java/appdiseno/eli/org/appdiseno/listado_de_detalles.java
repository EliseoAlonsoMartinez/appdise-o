package appdiseno.eli.org.appdiseno;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import appdiseno.eli.org.appdiseno.ArrayAdapter.CarritoAdapter;
import appdiseno.eli.org.appdiseno.Base.UsuarioDataBase;
import appdiseno.eli.org.appdiseno.DataBase.DataBase;

public class listado_de_detalles extends Activity implements AdapterView.OnItemClickListener {

    private CarritoAdapter adapter;
    private ArrayList<UsuarioDataBase> detalles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_de_detalles);

        detalles = new ArrayList<UsuarioDataBase>();
        DataBase db = new DataBase(getApplicationContext());
        detalles = db.getPrendas();
        adapter = new CarritoAdapter(this, R.layout.activity_listado_de_detalles, detalles);
        ListView lvDetalles = (ListView) findViewById(R.id.lvDetalles);
        lvDetalles.setAdapter(adapter);
        lvDetalles.setOnItemClickListener(listado_de_detalles.this);
        adapter.setNotifyOnChange(true);
        
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {

        Intent i = new Intent(this, modificar_detalles.class);
        i.putExtra("id", detalles.get(position).getId());
        startActivity(i);
    }
}
