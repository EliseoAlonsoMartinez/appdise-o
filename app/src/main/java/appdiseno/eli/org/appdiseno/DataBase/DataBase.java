package appdiseno.eli.org.appdiseno.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import appdiseno.eli.org.appdiseno.Base.UsuarioDataBase;

/**
 * Created by Eliseo on 07/06/2016.
 */
public class DataBase extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;

    private static final String DB_NAME = "UOU";

    private static final String TABLE_PRENDAS = "details";

    private static final String ID= "id";
    private static final String COL_MUNICIPIO = "municipio";
    private static final String COL_PROVINCIA = "provincia";
    private static final String COL_CALLE = "calle";
    private static final String COL_POSTAL = "cPostal";
    private static final String COL_TELEFONO = "telefono";
    private static final String COL_COMENTARIOS = "comentarios";


    public DataBase(Context context) {

        super(context, DB_NAME, null, DB_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_PRENDAS + "(" +
                ID + " INTEGER PRIMARY KEY," +
                COL_MUNICIPIO + " TEXT," +
                COL_PROVINCIA + " TEXT," +
                COL_POSTAL + " TEXT," +
                COL_CALLE + " TEXT," +
                COL_TELEFONO + " TEXT," +
                COL_COMENTARIOS + " TEXT " +")";
        db.execSQL(CREATE_USER_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void agregarPrenda(UsuarioDataBase prenda) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_COMENTARIOS, prenda.getComentarios());
        values.put(COL_CALLE, prenda.getCalle());
        values.put(COL_MUNICIPIO, prenda.getMunicipio());
        values.put(COL_TELEFONO, prenda.getTelefono());
        values.put(COL_PROVINCIA, prenda.getProvincia());
        values.put(COL_POSTAL, prenda.getcPostal());



        db.insert(TABLE_PRENDAS, null, values);
        db.close();
    }


    public ArrayList<UsuarioDataBase> getPrendas() {

        ArrayList<UsuarioDataBase> prendas = new ArrayList<UsuarioDataBase>();
        String selectQuery = "SELECT * FROM " + TABLE_PRENDAS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        while (cursor.moveToNext()) {

            UsuarioDataBase pren = new UsuarioDataBase(
                    cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5)
            );
            prendas.add(pren);
        }

        cursor.close();
        db.close();

        return prendas;
    }

    public void updatePrendas(UsuarioDataBase prenda, int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        ContentValues values = new ContentValues();


        values.put(COL_MUNICIPIO, prenda.getMunicipio());
        values.put(COL_PROVINCIA, prenda.getProvincia());
        values.put(COL_POSTAL, prenda.getcPostal());
        values.put(COL_CALLE, prenda.getCalle());
        values.put(COL_TELEFONO, prenda.getTelefono());
        values.put(COL_COMENTARIOS, prenda.getComentarios());

        String[] whereArgs = new String[]
                {
                        String.valueOf(id)
                };

        db.update(TABLE_PRENDAS, values, "id = ?", whereArgs);

        db.close();
    }
}
