package appdiseno.eli.org.appdiseno.Constantes;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import appdiseno.eli.org.appdiseno.Base.Correos;

/**
 * Created by Eliseo on 31/05/2016.
 */
public class ListaCorreos {


    public class TareaDescarga extends AsyncTask<String,Void,Void> {
        private boolean error = false;
        public ArrayList<Correos> listaCorreos=new ArrayList<>();
        private ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... strings) {
            String url = strings[0];
            InputStream is = null;
            String resultado = null;
            JSONObject json = null;
            JSONArray jsonArray = null;
            try {

                HttpClient clienteHtp = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                HttpResponse respuesta = clienteHtp.execute(httpPost);
                HttpEntity entity = respuesta.getEntity();
                is = entity.getContent();

                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String linea = null;
                while ((linea = br.readLine()) != null) {
                    sb.append(linea + "\n");
                }
                is.close();
                resultado = sb.toString();

                json = new JSONObject(resultado);
                jsonArray = json.getJSONArray("features");

                //String nombre2 = null;
                String coordenadas = null;
                Correos correos = null;

                for (int i = 0; i < jsonArray.length(); i++) {
                    //nombre2 = jsonArray.getJSONObject(i).getJSONObject("properties").getString("title");

                    coordenadas = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordenadas_p");
                    coordenadas = coordenadas.substring(1, coordenadas.length() - 1);
                    String latolong[] = coordenadas.split(",");

                    correos = new Correos();

                    correos.setLat(Float.parseFloat(latolong[0]));
                    correos.setLon(Float.parseFloat(latolong[1]));
                    listaCorreos.add(correos);
                }

            } catch (ClientProtocolException cpe) {
                cpe.printStackTrace();
                error = true;

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }
    }
}
