package appdiseno.eli.org.appdiseno;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;




import java.util.ArrayList;
import java.util.List;

import appdiseno.eli.org.appdiseno.Base.Prenda;
import appdiseno.eli.org.appdiseno.Constantes.Precio;


public class Empieza extends Activity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private Prenda prenda;
    private CajaCarrito cajaCarrito;
    private String cantidad_colores;

    private List<Prenda> listaPrendas;
    private TextView tfPrecios;
    public  static ArrayList<Prenda> listaPrenda = new ArrayList<Prenda>();
    private ArrayAdapter adapter;

    public Empieza(){
        listaPrendas=new ArrayList<>();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            cajaCarrito = new CajaCarrito();
            prenda = new Prenda();
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_empieza);
            tfPrecios = (TextView) findViewById(R.id.tfPrecio);


            final ImageView image = (ImageView) findViewById(R.id.imagenes);
            image.setImageResource(R.drawable.camiseta_relieve);

            final TextView tfPrecio = (TextView) findViewById(R.id.tfPrecio);
            tfPrecio.setOnClickListener(this);


            SeekBar barra_seguimiento = (SeekBar) findViewById(R.id.barra_seguimiento);
            barra_seguimiento.setOnSeekBarChangeListener(this);
            barra_seguimiento.setProgress(30);


            final Spinner sp1 = (Spinner) findViewById(R.id.sp1);
            ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.Tallas,
                    android.R.layout.simple_spinner_dropdown_item);
            sp1.setAdapter(adapter);

            sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        Toast.makeText(parent.getContext(), " Has seleccionado talla " + parent.getItemAtPosition(position).toString(),
                                Toast.LENGTH_SHORT).show();

                        prenda.setTalla(sp1.getSelectedItem().toString());
                        //System.out.println(prenda.getTalla().toString());
                        listaPrendas.add(prenda);


                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            final Spinner sp2 = (Spinner) findViewById(R.id.sp2);
            final ArrayAdapter adapter2 = ArrayAdapter.createFromResource(this, R.array.Color,
                    android.R.layout.simple_spinner_dropdown_item);
            sp2.setAdapter(adapter2);

            sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        Toast.makeText(parent.getContext(), " Has seleccionado color " + parent.getItemAtPosition(position).toString(),
                                Toast.LENGTH_SHORT).show();

                        prenda.setColor(sp2.getSelectedItem().toString());
                        //System.out.println(prenda.getColor().toString());
                        listaPrendas.add(prenda);
                    }

                    switch (position) {

                        case 1:
                            image.setImageResource(R.drawable.camiseta_blanco);
                            image.refreshDrawableState();
                            break;

                        case 2:
                            image.setImageResource(R.drawable.camiseta_negro);
                            image.refreshDrawableState();
                            break;

                        case 3:
                            image.setImageResource(R.drawable.camiseta_gris);
                            image.refreshDrawableState();
                            break;

                        case 4:
                            image.setImageResource(R.drawable.camiseta_azul);
                            image.refreshDrawableState();
                            break;

                        case 5:
                            image.setImageResource(R.drawable.camiseta_rojo);
                            image.refreshDrawableState();
                            break;

                        case 6:
                            image.setImageResource(R.drawable.camiseta_verde);
                            image.refreshDrawableState();
                            break;

                        case 7:
                            image.setImageResource(R.drawable.camiseta_naranja);
                            image.refreshDrawableState();
                            break;

                        case 8:
                            image.setImageResource(R.drawable.camiseta_rosa);
                            image.refreshDrawableState();
                            break;
                    }


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {


                }
            });

            final Spinner sp3 = (Spinner) findViewById(R.id.sp3);
            ArrayAdapter adapter3 = ArrayAdapter.createFromResource(this, R.array.Cantidad,
                    android.R.layout.simple_spinner_dropdown_item);
            sp3.setAdapter(adapter3);

            sp3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        Toast.makeText(parent.getContext(), " Has seleccionado " + parent.getItemAtPosition(position).toString() + " prendas",
                                Toast.LENGTH_SHORT).show();

                        prenda.setCantidad(Integer.parseInt(sp3.getSelectedItem().toString()));
                        //System.out.println(String.valueOf(prenda.getCantidad()).toString());
                        listaPrendas.add(prenda);
                        precios();


                    }


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            final Spinner sp4 = (Spinner) findViewById(R.id.sp4);
            final ArrayAdapter adapter4 = ArrayAdapter.createFromResource(this, R.array.Tipo_de_estampado,
                    android.R.layout.simple_spinner_dropdown_item);
            sp4.setAdapter(adapter4);

            sp4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        Toast.makeText(parent.getContext(), " El tipo de estampado es  " + parent.getItemAtPosition(position).toString(),
                                Toast.LENGTH_SHORT).show();

                        prenda.setEstampacion(sp4.getSelectedItem().toString());
                        //System.out.println(prenda.getEstampacion().toString());
                        listaPrendas.add(prenda);
                    }


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {


                }
            });

            final Spinner sp5 = (Spinner) findViewById(R.id.sp5);
            ArrayAdapter adapter5 = ArrayAdapter.createFromResource(this, R.array.Tipo_de_prenda,
                    android.R.layout.simple_spinner_dropdown_item);
            sp5.setAdapter(adapter5);

            sp5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        Toast.makeText(parent.getContext(), " Has seleccionado " + parent.getItemAtPosition(position).toString(),
                                Toast.LENGTH_SHORT).show();

                        prenda.setPrenda(sp5.getSelectedItem().toString());
                        //System.out.println(prenda.getPrenda().toString());
                        listaPrendas.add(prenda);
                        precios();
                        System.out.println(listaPrendas);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            final Spinner sp6 = (Spinner) findViewById(R.id.sp6);
            ArrayAdapter adapter6 = ArrayAdapter.createFromResource(this, R.array.Cantidad_colores,
                    android.R.layout.simple_spinner_dropdown_item);
            sp6.setAdapter(adapter6);

            sp6.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        Toast.makeText(parent.getContext(), " Has seleccionado " + parent.getItemAtPosition(position).toString() + " colores",
                                Toast.LENGTH_SHORT).show();

                        prenda.setCantidad_colores(Integer.parseInt(sp6.getSelectedItem().toString()));
                        //System.out.println(String.valueOf(prenda.getCantidad_colores()));
                        listaPrendas.add(prenda);
                        cantidad_colores = sp6.getSelectedItem().toString();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }

            });
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empieza, menu);
        return true;
    }

    @Override
    public void onProgressChanged(SeekBar barra_seguimiento, int progress, boolean fromUser) {
        try {


            int progresoBarra = barra_seguimiento.getProgress();
            if (progresoBarra <= 26) {
                barra_seguimiento.setProgress(0);
                Intent intent = new Intent(this, Entrada.class);
                startActivity(intent);
            }
            if ((progresoBarra > 32) && (progresoBarra < 55)) {
                barra_seguimiento.setProgress(50);
                Intent intent = new Intent(this, Carrito.class);
                startActivity(intent);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.search:
                Toast toas1=Toast.makeText(getApplicationContext(),"HOLA",Toast.LENGTH_LONG);
                toas1.show();
            case R.id.add:
                precios();
                try {


                    Intent intent = new Intent(getApplicationContext(), Carrito.class);

                    Bundle bundle1 = new Bundle();
                    Bundle bundle2 = new Bundle();
                    Bundle bundle3=new Bundle();

                    bundle1.putString("prenda", prenda.getPrenda());

                    String precio_prenda_carrito=Double.toString(prenda.getPrecio());
                    String cantidad_prenda_carrito=Integer.toString(prenda.getCantidad());

                    bundle2.putDouble("precio", Double.parseDouble(precio_prenda_carrito));
                    bundle3.putInt("cantidad", Integer.parseInt(cantidad_prenda_carrito));

                    intent.putExtras(bundle1);
                    intent.putExtras(bundle2);
                    intent.putExtras(bundle3);

                    startActivity(intent);
                }catch (Exception e){
                    e.printStackTrace();
                }

            default: return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {


    }



    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        //outState.putInt("CONT", );
    }

    @Override
    protected void onRestoreInstanceState(Bundle recEstado) {
        super.onRestoreInstanceState(recEstado);
        cantidad_colores = recEstado.getString(String.valueOf(prenda.getCantidad_colores()));
    }

    public void precios() {


        double precio_camiseta_manga_corta = Precio.precio_camiseta_manga_corta;
        double precio_camiseta_manga_larga = Precio.precio_camiseta_manga_larga;
        double precio_sudadera_con_bolsillo_capucha = Precio.precio_sudadera_capucha_bolsillo;
        double precio_sudadera_sin_bolsillo_capucha = Precio.precio_sudadera_sin_capucha_bolsillo;
        double precio_pantalon_corto = Precio.precio_pantalon_corto;
        double precio_pantalon_largo = Precio.precio_pantalon_largo;
        double precio_polo_manga_corta = Precio.precio_polo_manga_corta;
        double precio_gorra = Precio.precio_gorra;


        String tipo_prenda = String.valueOf(prenda.getPrenda());

//PRECIO A PARTIR DE 0 PRENDAS

        if (prenda.getCantidad() < 5) {
            if (tipo_prenda.equalsIgnoreCase("Camiseta manga corta")) {

                precio_camiseta_manga_corta = precio_camiseta_manga_corta * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_corta + "€");
            }

            if (tipo_prenda.equalsIgnoreCase("Camiseta manga larga")) {
                precio_camiseta_manga_larga = precio_camiseta_manga_larga * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_larga);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_larga + "€");

            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera con bolsillo/bolsillo")) {

                precio_sudadera_con_bolsillo_capucha = precio_sudadera_con_bolsillo_capucha * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_con_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_con_bolsillo_capucha + "€");
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera sin capucha")) {
                precio_sudadera_sin_bolsillo_capucha = precio_sudadera_sin_bolsillo_capucha * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_sin_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_sin_bolsillo_capucha + "€");

            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon corto")) {

                precio_pantalon_corto = precio_pantalon_corto * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_corto);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_corto + "€");
            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon largo")) {
                precio_pantalon_largo = precio_pantalon_largo * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_largo);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_largo + "€");

            }

            if (tipo_prenda.equalsIgnoreCase("Polo")) {

                precio_polo_manga_corta = precio_polo_manga_corta * prenda.getCantidad();
                prenda.setPrecio(precio_polo_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_polo_manga_corta + "€");
            }

            if (tipo_prenda.equalsIgnoreCase("Gorra")) {
                precio_gorra = precio_gorra * 0.97 * prenda.getCantidad();
                prenda.setPrecio(precio_gorra);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_gorra + "€");

            }


        }
//PRECIO A PARTIR DE 6 PRENDAS

        if ((prenda.getCantidad() > 5) && (prenda.getCantidad() <= 20)) {
            if (tipo_prenda.equalsIgnoreCase("Camiseta manga corta")) {

                precio_camiseta_manga_corta = precio_camiseta_manga_corta * 0.97 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER|Gravity.CENTER,0,0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Camiseta manga larga")) {
                precio_camiseta_manga_larga = precio_camiseta_manga_larga * 0.97 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_larga);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_larga + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_larga/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera con bolsillo/bolsillo")) {

                precio_sudadera_con_bolsillo_capucha = precio_sudadera_con_bolsillo_capucha * 0.97 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_con_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_con_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_con_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera sin capucha")) {
                precio_sudadera_sin_bolsillo_capucha = precio_sudadera_sin_bolsillo_capucha * 0.97 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_sin_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_sin_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_sin_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon corto")) {

                precio_pantalon_corto = precio_pantalon_corto * 0.97 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_corto);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_corto + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_corto/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon largo")) {
                precio_pantalon_largo = precio_pantalon_largo * 0.97 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_largo);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_largo + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_largo/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Polo")) {

                precio_polo_manga_corta = precio_polo_manga_corta * 0.97 * prenda.getCantidad();
                prenda.setPrecio(precio_polo_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_polo_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El polo sale a " + precio_polo_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Gorra")) {
                precio_gorra = precio_gorra * 0.97 * prenda.getCantidad();
                prenda.setPrecio(precio_gorra);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_gorra + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La gorra sale a " + precio_gorra/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }
        }

//PRECIO A PARTIR DE 21 PRENDAS

        if ((prenda.getCantidad() > 20) && (prenda.getCantidad() <= 30)) {
            if (tipo_prenda.equalsIgnoreCase("Camiseta manga corta")) {

                precio_camiseta_manga_corta = precio_camiseta_manga_corta * 0.95 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Camiseta manga larga")) {
                precio_camiseta_manga_larga = precio_camiseta_manga_larga * 0.95 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_larga);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_larga + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_larga/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera con bolsillo/bolsillo")) {

                precio_sudadera_con_bolsillo_capucha = precio_sudadera_con_bolsillo_capucha * 0.95 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_con_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_con_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_con_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera sin capucha")) {
                precio_sudadera_sin_bolsillo_capucha = precio_sudadera_sin_bolsillo_capucha * 0.95 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_sin_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_sin_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_sin_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon corto")) {

                precio_pantalon_corto = precio_pantalon_corto * 0.95 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_corto);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_corto + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_corto/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon largo")) {
                precio_pantalon_largo = precio_pantalon_largo * 0.95 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_largo);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_largo + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_largo/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Polo")) {

                precio_polo_manga_corta = precio_polo_manga_corta * 0.95 * prenda.getCantidad();
                prenda.setPrecio(precio_polo_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_polo_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El polo sale a " + precio_polo_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Gorra")) {
                precio_gorra = precio_gorra * 0.97 * prenda.getCantidad();
                prenda.setPrecio(precio_gorra);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_gorra + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La gorra sale a" + precio_gorra/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }


        }

//PRECIO A PARTIR DE 31 PRENDAS

        if ((prenda.getCantidad() > 31) && (prenda.getCantidad() <= 50)) {
            if (tipo_prenda.equalsIgnoreCase("Camiseta manga corta")) {

                precio_camiseta_manga_corta = precio_camiseta_manga_corta * 0.93 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Camiseta manga larga")) {
                precio_camiseta_manga_larga = precio_camiseta_manga_larga * 0.93 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_larga);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_larga + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_larga/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera con bolsillo/bolsillo")) {

                precio_sudadera_con_bolsillo_capucha = precio_sudadera_con_bolsillo_capucha * 0.93 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_con_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_con_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_con_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera sin capucha")) {
                precio_sudadera_sin_bolsillo_capucha = precio_sudadera_sin_bolsillo_capucha * 0.93 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_sin_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_sin_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_sin_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon corto")) {

                precio_pantalon_corto = precio_pantalon_corto * 0.93 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_corto);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_corto + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_corto/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon largo")) {
                precio_pantalon_largo = precio_pantalon_largo * 0.93 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_largo);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_largo + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_largo/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Polo")) {

                precio_polo_manga_corta = precio_polo_manga_corta * 0.93 * prenda.getCantidad();
                prenda.setPrecio(precio_polo_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_polo_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El polo sale a " + precio_polo_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Gorra")) {
                precio_gorra = precio_gorra * 0.93 * prenda.getCantidad();
                prenda.setPrecio(precio_gorra);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_gorra + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La gorra sale a " + precio_gorra/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }
        }

//PRECIO A PARTIR DE 51 PRENDAS

        if ((prenda.getCantidad() > 51) && (prenda.getCantidad() <= 100)) {
            if (tipo_prenda.equalsIgnoreCase("Camiseta manga corta")) {

                precio_camiseta_manga_corta = precio_camiseta_manga_corta * 0.9 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Camiseta manga larga")) {
                precio_camiseta_manga_larga = precio_camiseta_manga_larga * 0.9 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_larga);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_larga + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_larga/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera con bolsillo/bolsillo")) {

                precio_sudadera_con_bolsillo_capucha = precio_sudadera_con_bolsillo_capucha * 0.9 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_con_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_con_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_con_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera sin capucha")) {
                precio_sudadera_sin_bolsillo_capucha = precio_sudadera_sin_bolsillo_capucha * 0.9 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_sin_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_sin_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_sin_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon corto")) {

                precio_pantalon_corto = precio_pantalon_corto * 0.9 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_corto);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_corto + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_corto/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon largo")) {
                precio_pantalon_largo = precio_pantalon_largo * 0.9 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_largo);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_largo + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_largo/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Polo")) {

                precio_polo_manga_corta = precio_polo_manga_corta * 0.9 * prenda.getCantidad();
                prenda.setPrecio(precio_polo_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_polo_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El polo sale a " + precio_polo_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Gorra")) {
                precio_gorra = precio_gorra * 0.9 * prenda.getCantidad();
                prenda.setPrecio(precio_gorra);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_gorra + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La gorra sale a " + precio_gorra/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

        }

//PRECIO A PARTIR DE 101 PRENDAS

        if ((prenda.getCantidad() > 101) && (prenda.getCantidad() <= 300)) {
            if (tipo_prenda.equalsIgnoreCase("Camiseta manga corta")) {

                precio_camiseta_manga_corta = precio_camiseta_manga_corta * 0.87 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Camiseta manga larga")) {
                precio_camiseta_manga_larga = precio_camiseta_manga_larga * 0.87 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_larga);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_larga + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_larga/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera con bolsillo/bolsillo")) {

                precio_sudadera_con_bolsillo_capucha = precio_sudadera_con_bolsillo_capucha * 0.87 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_con_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_con_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_con_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera sin capucha")) {
                precio_sudadera_sin_bolsillo_capucha = precio_sudadera_sin_bolsillo_capucha * 0.87 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_sin_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_sin_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_sin_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon corto")) {

                precio_pantalon_corto = precio_pantalon_corto * 0.87 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_corto);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_corto + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_corto/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon largo")) {
                precio_pantalon_largo = precio_pantalon_largo * 0.87 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_largo);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_largo + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_largo/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Polo")) {

                precio_polo_manga_corta = precio_polo_manga_corta * 0.87 * prenda.getCantidad();
                prenda.setPrecio(precio_polo_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_polo_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El polo sale a " + precio_polo_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Gorra")) {
                precio_gorra = precio_gorra * 0.8 * prenda.getCantidad();
                prenda.setPrecio(precio_gorra);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_gorra + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La gorra sale a " + precio_gorra/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }
        }
//PRECIO A PARTIR DE 301 PRENDAS

        if ((prenda.getCantidad() > 301) && (prenda.getCantidad() <= 500)) {
            if (tipo_prenda.equalsIgnoreCase("Camiseta manga corta")) {

                precio_camiseta_manga_corta = precio_camiseta_manga_corta * 0.8 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Camiseta manga larga")) {
                precio_camiseta_manga_larga = precio_camiseta_manga_larga * 0.8 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_larga);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_larga + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_larga/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera con bolsillo/bolsillo")) {

                precio_sudadera_con_bolsillo_capucha = precio_sudadera_con_bolsillo_capucha * 0.8 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_con_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_con_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_con_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera sin capucha")) {
                precio_sudadera_sin_bolsillo_capucha = precio_sudadera_sin_bolsillo_capucha * 0.8 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_sin_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_sin_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_sin_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon corto")) {

                precio_pantalon_corto = precio_pantalon_corto * 0.8 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_corto);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_corto + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_corto/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon largo")) {
                precio_pantalon_largo = precio_pantalon_largo * 0.8 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_largo);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_largo + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_largo/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Polo")) {

                precio_polo_manga_corta = precio_polo_manga_corta * 0.8 * prenda.getCantidad();
                prenda.setPrecio(precio_polo_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_polo_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El polo sale a " + precio_polo_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Gorra")) {
                precio_gorra = precio_gorra * 0.8 * prenda.getCantidad();
                prenda.setPrecio(precio_gorra);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_gorra + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La gorra sale a " + precio_gorra/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }
        }

//PRECIO A PARTIR DE 501 PRENDAS

        if ((prenda.getCantidad() > 501) && (prenda.getCantidad() <= 1000)) {
            if (tipo_prenda.equalsIgnoreCase("Camiseta manga corta")) {

                precio_camiseta_manga_corta = precio_camiseta_manga_corta * 0.7 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Camiseta manga larga")) {
                precio_camiseta_manga_larga = precio_camiseta_manga_larga * 0.7 * prenda.getCantidad();
                prenda.setPrecio(precio_camiseta_manga_larga);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_camiseta_manga_larga + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La camiseta sale a " + precio_camiseta_manga_larga/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera con bolsillo/bolsillo")) {

                precio_sudadera_con_bolsillo_capucha = precio_sudadera_con_bolsillo_capucha * 0.7 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_con_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_con_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_con_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Sudadera sin capucha")) {
                precio_sudadera_sin_bolsillo_capucha = precio_sudadera_sin_bolsillo_capucha * 0.7 * prenda.getCantidad();
                prenda.setPrecio(precio_sudadera_sin_bolsillo_capucha);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_sudadera_sin_bolsillo_capucha + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La sudadera sale a " + precio_sudadera_sin_bolsillo_capucha/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();

            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon corto")) {

                precio_pantalon_corto = precio_pantalon_corto * 0.7 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_corto);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_corto + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_corto/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Pantalon largo")) {
                precio_pantalon_largo = precio_pantalon_largo * 0.7 * prenda.getCantidad();
                prenda.setPrecio(precio_pantalon_largo);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_pantalon_largo + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El pantalon sale a " + precio_pantalon_largo/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Polo")) {

                precio_polo_manga_corta = precio_polo_manga_corta * 0.7 * prenda.getCantidad();
                prenda.setPrecio(precio_polo_manga_corta);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_polo_manga_corta + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "El polo sale a " + precio_polo_manga_corta/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }

            if (tipo_prenda.equalsIgnoreCase("Gorra")) {
                precio_gorra = precio_gorra * 0.7 * prenda.getCantidad();
                prenda.setPrecio(precio_gorra);
                listaPrendas.add(prenda);
                tfPrecios.setText(" PRECIO: " + precio_gorra + "€");
                Toast toast1=Toast.makeText(getApplicationContext(), "La gorra sale a " + precio_gorra/prenda.getCantidad(), BIND_IMPORTANT);
                toast1.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
                toast1.show();
            }
        }
    }



}






