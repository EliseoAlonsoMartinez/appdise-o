package appdiseno.eli.org.appdiseno;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;

import appdiseno.eli.org.appdiseno.Base.Prenda;

public class Carrito extends Activity implements View.OnClickListener,SeekBar.OnSeekBarChangeListener{

    private ListView lvPrendas;
   // private AdapterCarrito adapter;
    private ArrayList<Prenda> listaprendas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito);


        Button btAceptarCarrito=(Button) findViewById(R.id.btAceptarCarrito);
        btAceptarCarrito.setOnClickListener(this);
        TextView tfPrenda=(TextView) findViewById(R.id.tfPrenda);
        TextView tfPrecio=(TextView) findViewById(R.id.tfPrecio);
        TextView tfCantidad=(TextView) findViewById(R.id.tfCantidad);
        TextView tfPrecioTotalCarrito=(TextView) findViewById(R.id.tfPrecioTotalCarrito);
        ImageView imageView=(ImageView) findViewById(R.id.imagenPrenda);
        imageView.setImageResource(R.drawable.camiseta_relieve);

        try {
            Bundle prenda=getIntent().getExtras();
            Bundle precio=this.getIntent().getExtras();
            Bundle cantidad=this.getIntent().getExtras();

            double precios=precio.getDouble("precio");
            int cantidades=cantidad.getInt("cantidad");

            tfPrecio.setText(String.valueOf(precios + " €"));
            tfPrecioTotalCarrito.setText(String.valueOf(precios + "€"));
            tfPrenda.setText(prenda.getString("prenda"));
            tfCantidad.setText(String.valueOf("x " + cantidades + "  "));

        }catch (Exception e){
            e.printStackTrace();
        }





        SeekBar barra_seguimiento=(SeekBar) findViewById(R.id.barra_seguimiento);
        barra_seguimiento.setOnSeekBarChangeListener(this);
        barra_seguimiento.setProgress(50);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_carrito, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressChanged(SeekBar barra_seguimiento, int progress, boolean fromUser) {
        int progresoBarra=barra_seguimiento.getProgress();

        if((progresoBarra > 26 ) && (progresoBarra < 40)){
            barra_seguimiento.setProgress(30);
            Intent intent=new Intent(this,Empieza.class);
            startActivity(intent);
        }

        if((progresoBarra<85) && (progresoBarra> 60)){
            barra_seguimiento.setProgress(80);
            Intent intent1=new Intent(this,Envio.class);
            startActivity(intent1);
        }


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btAceptarCarrito:
                Intent intent = new Intent(this,Envio.class);
                startActivity(intent);
                break;

        }
    }
}
