package appdiseno.eli.org.appdiseno;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import appdiseno.eli.org.appdiseno.Base.UsuarioDataBase;
import appdiseno.eli.org.appdiseno.DataBase.DataBase;

public class Envio extends Activity implements View.OnClickListener,SeekBar.OnSeekBarChangeListener{

    public DataBase db;
    public EditText etMuncipio;
    public EditText etProvincia;
    public EditText etCalle;
    public EditText etcPostal;
    public EditText ettelefono;
    public EditText etComentarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_envio);

        SeekBar barra_seguimiento=(SeekBar) findViewById(R.id.progresoBarra);
        barra_seguimiento.setOnSeekBarChangeListener(this);
        barra_seguimiento.setProgress(80);

        db = new DataBase(getApplicationContext());

        Button btEnviar  = (Button) findViewById(R.id.btEnviarDet);
        etMuncipio = (EditText) findViewById(R.id.etMunicipio2);
        etProvincia = (EditText) findViewById(R.id.etProvincia);
        etCalle = (EditText) findViewById(R.id.etCalle);
        etcPostal = (EditText) findViewById(R.id.etcPostal);
        ettelefono = (EditText)findViewById(R.id.etTelefono);
        etComentarios = (EditText)findViewById(R.id.etComentarios);

        btEnviar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                UsuarioDataBase user = new UsuarioDataBase(
                        etMuncipio.getText().toString(),
                        etProvincia.getText().toString(),
                        etCalle.getText().toString(),
                        etcPostal.getText().toString(),
                        ettelefono.getText().toString(),
                        etComentarios.getText().toString()
                );

                user.setCalle(etCalle.getText().toString());
                user.setCalle(etProvincia.getText().toString());
                user.setCalle(etMuncipio.getText().toString());
                user.setCalle(etcPostal.getText().toString());
                user.setCalle(ettelefono.getText().toString());
                user.setCalle(etComentarios.getText().toString());

                db.agregarPrenda(user);
                System.out.println(user.getCalle());
                System.out.println(user.getMunicipio());
                System.out.println(user.getProvincia());
                Toast.makeText(getApplicationContext(), "Detalles enviados", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Envio.this, Entrada.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_envio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int progresoBarra=seekBar.getProgress();

        if((progresoBarra > 45) && (progresoBarra < 75)){
            seekBar.setProgress(50);
            Intent intent=new Intent(this,Carrito.class);
            startActivity(intent);
        }

        if((progresoBarra > 83)){
            seekBar.setProgress(100);
            Intent intent=new Intent(this,Pago.class);
            startActivity(intent);
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {

    }
}

