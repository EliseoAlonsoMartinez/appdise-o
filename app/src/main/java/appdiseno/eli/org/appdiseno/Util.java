package appdiseno.eli.org.appdiseno;

import com.mapbox.mapboxsdk.geometry.LatLng;

/**
 * Created by Eliseo on 31/05/2016.
 */
public class Util {
   public static uk.me.jstott.jcoord.LatLng DeUMTSaLatLng(double este, double oeste) {


        uk.me.jstott.jcoord.UTMRef utm = new uk.me.jstott.jcoord.UTMRef(este, oeste, 'N', 30);

        return utm.toLatLng();
    }



    public static LatLng parseCoordenadas(String lat,String longitud) {

        // Convierte las coordenadas de UTM a Latitud y Longitud de la librería jcoord
        uk.me.jstott.jcoord.LatLng ubicacion = Util.DeUMTSaLatLng(Double.parseDouble(lat),Double.parseDouble(longitud));

        // Devuelve finalmente las coordenadas como un objeto LatLng de Google Maps
        return new LatLng(ubicacion.getLat(), ubicacion.getLng());
    }


}
