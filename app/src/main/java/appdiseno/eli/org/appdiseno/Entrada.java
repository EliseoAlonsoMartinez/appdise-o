package appdiseno.eli.org.appdiseno;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

public class Entrada extends Activity implements View.OnClickListener,SeekBar.OnSeekBarChangeListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada);

        Button btDiseño=(Button) findViewById(R.id.btDiseno);
        btDiseño.setOnClickListener(this);

        Button btSticks=(Button) findViewById(R.id.btSticks);
        btSticks.setOnClickListener(this);

        Button btLista=(Button) findViewById(R.id.btListaDeDetalles);
        btLista.setOnClickListener(this);

        Button btEntrarConCuenta=(Button) findViewById(R.id.btEntrarConCuenta);
        btEntrarConCuenta.setOnClickListener(this);

        Button btRegistrarse=(Button) findViewById(R.id.btRegistrarse);
        btRegistrarse.setOnClickListener(this);



        ImageView img=(ImageView) findViewById(R.id.imageView);
        img.setImageResource(R.drawable.tu_nombre_aqui);



        SeekBar barra_seguimiento=(SeekBar) findViewById(R.id.barra_seguimiento);
        barra_seguimiento.setOnSeekBarChangeListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_entrada, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btDiseno:
                Intent intent = new Intent(this,Mapa.class);
                startActivity(intent);
                break;
            case R.id.btRegistrarse:
                startActivity(new Intent(this, Registro.class));
                break;
            case R.id.btEntrarConCuenta:
                startActivity(new Intent(this, Login.class));
                break;
            case R.id.btListaDeDetalles:
                startActivity(new Intent(this, listado_de_detalles.class));

        }
    }

    @Override
    public void onProgressChanged(SeekBar barra_seguimiento, int progress, boolean fromUser) {
        int progresoBarra=barra_seguimiento.getProgress();
        if((progresoBarra>10)&& (progresoBarra<45)){
            barra_seguimiento.setProgress(30);
            Intent intent=new Intent(this,Empieza.class);
            startActivity(intent);
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
