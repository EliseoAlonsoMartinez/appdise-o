package appdiseno.eli.org.appdiseno.Base;

import java.util.ArrayList;

/**
 * Created by Eliseo on 31/05/2016.
 */
public class Correos {
    private double lat;
    private double lon;
    private String nombre;

    public String getNombre() {
        return nombre;

    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "Correos{" +
                "lat=" + lat +
                ", lon=" + lon +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
